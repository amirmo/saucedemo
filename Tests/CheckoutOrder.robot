*** Settings ***
Resource    ../Resources/PageObjectModel/CartPagePOM.robot
Resource    ../Resources/PageObjectModel/CheckOutPOM.robot


*** Test Cases ***

Click Cart Icon
    Click on Shopping Cart Icon

Navigate to Checkout Page
    Click on CheckOut Button

Fill Checkout Form
    Enter First Name    Amir
    Enter Last Name     Mohey
    Enter Postal Code   1001

Continue over Checkout Steps
    Click Continue Button
    Click Finish Button
    Verify that Order is Completed

