*** Settings ***
Resource    ../CommonResources.robot
*** Variables ***
${ShoppingCartIcon}     class=shopping_cart_link
${CheckoutBTN}          id=checkout

*** Keywords ***
Click on Shopping Cart Icon
    wait until page contains element    ${ShoppingCartIcon}
    click link    ${ShoppingCartIcon}
    location should be    https://www.saucedemo.com/cart.html
    capture page screenshot    Screenshots/cartPage.png


Click on CheckOut Button
    wait until page contains element    ${CheckoutBTN}
    click button    ${CheckoutBTN}
    location should be    https://www.saucedemo.com/checkout-step-one.html