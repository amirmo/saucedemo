*** Settings ***
Resource    ../CommonResources.robot
*** Variables ***
${ShoppingCartIcon}         class=shopping_cart_link
${CheckoutBTN}              id=checkout
${FirstNameField}           id=first-name
${LastNameField}            id=last-name
${postalCodeField}          id=postal-code
${ContinueBTN}              id=continue
${FinishBTN}                id=finish
${CompletedOrderMessage}    class=complete-header

*** Keywords ***
Enter First Name
    [Arguments]    ${FirstName}
    wait until page contains element    ${FirstNameField}
    input text    ${FirstNameField}    ${FirstName}
Enter Last Name
    [Arguments]    ${LastName}
    input text    ${LastNameField}    ${LastName}
Enter Postal Code
    [Arguments]    ${PostalCode}
    input text    ${postalCodeField}     ${PostalCode}

Click Continue Button
    click button    ${ContinueBTN}
    location should be    https://www.saucedemo.com/checkout-step-two.html

Click Finish Button
    click button    ${FinishBTN}

Verify that Order is Completed
    location should be    https://www.saucedemo.com/checkout-complete.html
    element should contain    ${CompletedOrderMessage}     THANK YOU FOR YOUR ORDER