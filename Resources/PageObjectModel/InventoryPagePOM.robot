*** Settings ***
Resource    ../CommonResources.robot

*** Variables ***
${FirstItemAddToCart}       //div[@class="inventory_item"][1]//button[contains(text(),'Add to cart')]
${RemoveItemFromCartBTN}    id = remove-sauce-labs-backpack
${CartItemsNumber}          xpath = //div[@id="shopping_cart_container"]//a/span


*** Keywords ***
Add First Item To Cart

    click button    ${FirstItemAddToCart}
    element text should be    ${RemoveItemFromCartBTN}    REMOVE
    element text should be    ${CartItemsNumber}        1
    capture page screenshot     Screenshots/InventoryPage.png