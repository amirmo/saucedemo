*** Settings ***
Resource    ../CommonResources.robot
*** Variables ***
${UserName}         id=user-name
${Password}         id=password
${LogInBTN}         id=login-button

*** Keywords ***
Enter User Name
       [Arguments]    ${usernme}
       input text    ${UserName}    ${usernme}

Enter Password
        [Arguments]    ${userPass}
       input text    ${Password}    ${userPass}

Click LogIn Button
        click button    ${LogInBTN}